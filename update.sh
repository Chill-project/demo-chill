git pull origin master

# remove all image
# docker rmi $(docker images -a -q)

docker pull chill/database
docker pull chill/demo-flavor

# remove all container
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")

docker run -d --name=chill_database chill/database
sleep 5
docker run -d --link chill_database:db -p 8989:8000 --name=chill_php -v /var/www/demo-chill/github/app/Resources/:/var/www/chill/app/Resources:ro  chill/demo-flavor